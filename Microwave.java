public class Microwave
{
	private String brand;
	private String explosionColor;
	private double cookTime;
	
	//constructor
	public Microwave(String brand, String explosionColor, double cookTime)
	{
		this.brand = brand;
		this.explosionColor = explosionColor;
		this.cookTime = cookTime;
	}
	
	//setters

	public void setExplosionColor(String explosionColor)
	{
		this.explosionColor = explosionColor;
	}
	public void setCookTime(double cookTime)
	{
		this.cookTime = cookTime;
	}
	
	//getters
	public String getBrand()
	{
		return this.brand;
	}
	public String getExplosionColor()
	{
		return this.explosionColor;
	}
	public double getCookTime()
	{
		return this.cookTime;
	}
	
	//instance methods
	public void printBrand(String brand)
	{
		System.out.println("Are you a " + brand + " because you warm up my spagetthi");
			//method to print the microwave brand
	}
	public double cookTiming(double time)
	{
		return time+=0.3;
		//0.3 minutes extra to let the food cool off so you can take the food out of the microwave
	}
	public void bOOM(double sizeOfExplosion)
	{
		if(isBoomin(sizeOfExplosion)){
			this.explosionColor = "RAINBOW";
		}
	}
	private boolean isBoomin(double sizeOfExplosion)
	{
		if(sizeOfExplosion > 0){
			return true;
		}
		
		return false;
	}
}
	