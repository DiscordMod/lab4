public class ApplianceStore
{
	public static void main(String[]args)
	{
		java.util.Scanner reader = new java.util.Scanner(System.in);
		
		Microwave[] microwave = new Microwave[4];
		
		for(int i = 0; i < microwave.length; i++){
		
			System.out.println("What drip is your microwave on?");
			String brand = reader.next();
			
			System.out.println("is your microwave on?");
			double cookTime = reader.nextDouble();
			
			System.out.println("What colors of the rainbow is your microwave?");
			String explosionColor = reader.next();
			
			microwave[i] = new Microwave(brand, explosionColor, cookTime);
		}
		
		System.out.println(microwave[3].getBrand());
		System.out.println(microwave[3].getCookTime());
		System.out.println(microwave[3].getExplosionColor());
		
		double cookingTime = reader.nextDouble();
		microwave[3].setCookTime(cookingTime);
		
		String explosiveColors = reader.next();
		microwave[3].setExplosionColor(explosiveColors);
		
		System.out.println(microwave[3].getBrand());
		System.out.println(microwave[3].getCookTime());
		System.out.println(microwave[3].getExplosionColor());
		
		Microwave microwaveablePasta = new Microwave("Toshiba", "TURQUOISE", 20);
		microwaveablePasta.printBrand(microwave[0].getBrand());  
		System.out.println(microwaveablePasta.cookTiming(microwave[0].getCookTime()));  
		
		System.out.println(microwave[1].getExplosionColor());
		microwave[1].bOOM(5);
		System.out.println(microwave[1].getExplosionColor());
		
	}
}
